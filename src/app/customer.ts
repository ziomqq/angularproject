import { Address } from '../app/address';
import { Document } from '../app/document';

export class Customer{
    public id: Number;
    public firstName: String;
    public surname: String;
    public phone: String;
    public email: String;
    public bornDate: Date;
    public company: String;
    public gender: String;
    public address: Address;
    public document: Document;

    constructor(){
        this.address = new Address();
        this.document = new Document();
    }
}