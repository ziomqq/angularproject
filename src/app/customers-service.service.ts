import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Customer } from './customer';

@Injectable({
  providedIn: 'root'
})
export class CustomersServiceService {
  public arr: Array<Customer>;
  public toogle: EventEmitter<any> = new EventEmitter();

  constructor(private http: HttpClient) {
    this.requestData();
  }

  requestData() {
    this.http.get<Array<Customer>>('https://demo6180277.mockable.io/customers').subscribe(data => {
      this.arr = data;
      this.toogle.emit(this.arr);
    });
  }



}
