import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomerDetailsComponent } from './app-component/customer-details/customer-details.component';
import { CustomerAddressComponent } from './app-component/customer-address/customer-address.component';
import { CustomersServiceService } from './customers-service.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { IntlModule } from '@progress/kendo-angular-intl';
import { FormsModule } from '../../node_modules/@angular/forms';
import { PopupModule } from '@progress/kendo-angular-popup';


@NgModule({
  declarations: [
    AppComponent,
    CustomerDetailsComponent,
    CustomerAddressComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    GridModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    IntlModule,
    PopupModule
  ],
  providers: [CustomersServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
