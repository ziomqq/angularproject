import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Customer } from '../../customer';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent{
  @Input('selectedCustomer') selectedCustomer: Customer;

  constructor() { }

}
