import { Component, Input } from '@angular/core';
import { Customer } from 'src/app/customer';

@Component({
  selector: 'app-customer-address',
  templateUrl: './customer-address.component.html',
  styleUrls: ['./customer-address.component.css']
})
export class CustomerAddressComponent {
  @Input('selectedCustomer')  selectedCustomer: Customer;
  constructor() {}
}
