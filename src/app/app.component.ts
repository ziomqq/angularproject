import { Component } from '@angular/core';
import { CustomersServiceService } from './customers-service.service'
import { Customer } from './customer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  private show: boolean = false;
  public selectedCustomer: Customer = new Customer();
  private filterContent: String = "";
  private tempArrCustomer: Customer[];

  constructor(private __customerService: CustomersServiceService) {
    __customerService.toogle.subscribe(data => {this.tempArrCustomer = data})
  }
  title = 'app';

  public eventSelect(row) {
    this.selectedCustomer = row.selectedRows[0].dataItem;
  }

  public onToggle(): void {
    this.show = !this.show;
  }

  public onChangeFilter() {
    if (this.filterContent != "") {
      this.tempArrCustomer = this.__customerService.arr.filter(element => element.firstName.toUpperCase().includes(this.filterContent.toUpperCase()));
    }
    else {
      this.tempArrCustomer = this.__customerService.arr;
    }
  }
}
